import './assets/main.css'

import {createApp} from 'vue'
import {createRouter, createWebHashHistory} from 'vue-router'
import App from './App.vue'

import Home from './components/Home.vue'
import Projects from './components/Projects.vue'
import About from './components/About.vue'
import Members from './components/Members.vue'

const routes = [
    {path: '/', component: Home},
    {path: '/projects', component: Projects},
    {path: '/about', component: About},
    {path: '/members', component: Members},
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
})

const app = createApp(App)
app.use(router)
app.mount('#app')
